<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'middleware' => 'api'
], function ($router) {

    Route::resource('actividades','ActividadController');
    Route::resource('actividadprogreso','ActividadProgresoController');
    Route::resource('calificaciones','CalificacionController');
    Route::resource('ciclos','CicloController');
    Route::post('ciclos/attachCourse/{id}','CicloController@attachCourseToCiclo');
    Route::resource('comentarios','ComentarioController');
    Route::resource('cursos','CursoController');
    Route::post('cursos/createNbind/{id}','CursoController@createNbind');
    Route::post('cursos/unbindCiclo/{id}','CursoController@unbind');
    Route::resource('cursosusers','ComentarioController');
    Route::resource('estadosactividades','EstadoActividadController');
    Route::resource('estados','EstadoController');
    Route::resource('estadosprogreso','EstadoProgresoController');
    Route::resource('laboratorios','LaboratorioController');
    Route::get('users/profesores', 'AuthController@getProfesores');

    Route::post('users/profesores/createNbind/{id}','AuthController@createNbind');
    Route::post('users/profesores/unbindProf/{id}','AuthController@unbind');
    Route::post('cursos/attachProf/{id}','CursoController@attachProfToCourse');

    Route::post('teahcer/attachToCourse/{id}','AuthController@attachProfToCourse');

    Route::post('teacher/create','AuthController@createProf');
    Route::post('teacher/updateProf/{id}','AuthController@updateProf');
    Route::post('teacher/delete/{id}','AuthController@deleteProf');

    Route::get('users/students', 'AuthController@getStudents');
    Route::post('student/delete/{id}', 'AuthController@deleteStudent');
    Route::post('student/create', 'AuthController@createStudent');
    Route::post('student/updateStudent/{id}', 'AuthController@updateStudent');
    Route::post('student/attachToCourse/{id}', 'AuthController@attachStudentToCourse');
    Route::post('users/students/unbind/{id}', 'AuthController@unbindStudentToCourse');

    Route::get('users/teacher/courses/{id}', 'CursoController@getCoursesByTeacher');
    
    Route::get('users/student/courses/{id}', 'CursoController@getCoursesByStudent');

    Route::get('lab','LaboratorioController@testGNS3');

    Route::get('lab2','LaboratorioController@testGNS32');

    Route::post('users/students/startLab', 'LaboratorioController@iniciarProyecto');

    Route::post('users/students/continueLab', 'LaboratorioController@continuarProyecto');

    Route::post('users/students/addc7200', 'LaboratorioController@nodoc7200');
    
    Route::get('users/students/nodes/{proyect_id}', 'LaboratorioController@getNodes');
    
    Route::post('users/students/nodes/{proyect_id}/changePos/{node_id}', 'LaboratorioController@changeDevicePos');

    Route::get('users/students/links/{proyect_id}', 'LaboratorioController@getLinks');

    Route::get('users/students/lab/{proyect_id}', 'LaboratorioController@getLabData');

    Route::post('users/students/link/{proyect_id}', 'LaboratorioController@linkNodes');

    Route::post('users/students/linkDel/{proyect_id}/{link_id}', 'LaboratorioController@deleteLink');

    Route::post('users/students/lab/{proyect_id}/node/{node_id}/turnOn', 'LaboratorioController@turnOn');

    Route::post('users/students/lab/{proyect_id}/node/{node_id}/turnOff', 'LaboratorioController@turnOff');

    Route::post('users/students/lab/console', 'LaboratorioController@OpenConsole');


});
