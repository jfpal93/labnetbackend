<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $user1 = User::create(['name' => "Josy",'lastname'=>"Quimis", 'email'=> 'josy@gmail.com', 'password' => Hash::make('12345678')]);
        $user2 = User::create(['name' => "Jorge",'lastname'=>"Fierro", 'email'=> 'jorge@gmail.com', 'password' => Hash::make('12345678')]);
        $user3 = User::create(['name' => "Andres",'lastname'=>"Cervantes", 'email'=> 'andres@gmail.com', 'password' => Hash::make('12345678')]);

        $admin = Role::create(['name' => "admin"]);
        $user1->assignRole($admin);

        $prof = Role::create(['name' => "teachers"]);
        $user2->assignRole($prof);

        $student = Role::create(['name' => "student"]);
        $user3->assignRole($student);

    }
}
