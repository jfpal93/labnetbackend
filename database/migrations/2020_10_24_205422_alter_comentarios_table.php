<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('comentarios', function(Blueprint $table) {
            $table->dropForeign('comentarios_actividad_id_foreign');
            $table->dropForeign('comentarios_user_id_foreign');

            
            $table->dropColumn('user_id');
            $table->dropColumn('actividad_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
