<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadosActividadestable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_actividades', function (Blueprint $table) {
            $table->id();

            //Referencia actividad
            $table->bigInteger('actividad_id')->unsigned()->nullable();
            $table->foreign('actividad_id')->references('id')->on('actividades')->onDelete('cascade');

            //Referencia actividad progreso
            $table->bigInteger('estados_id')->unsigned()->nullable();
            $table->foreign('estados_id')->references('id')->on('estados')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_actividadestable');
    }
}
