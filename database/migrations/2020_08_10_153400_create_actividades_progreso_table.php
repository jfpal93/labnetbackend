<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadesProgresoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades_progreso', function (Blueprint $table) {
            $table->id();
            $table->timestamp("fecha_progreso")->nullable();
            $table->string('resultado')->nullable();

            //Referencia actividad
            $table->bigInteger('actividad_id')->unsigned()->nullable();
            $table->foreign('actividad_id')->references('id')->on('actividades')->onDelete('cascade');

            //Referencia users
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Referencia actividad progreso
            $table->bigInteger('estado_progreso_id')->unsigned()->nullable();
            $table->foreign('estado_progreso_id')->references('id')->on('estados_progreso')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades_progreso');
    }
}
