<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyActividadesProgresoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('actividades_progreso', function(Blueprint $table) {
            $table->dropForeign('actividades_progreso_user_id_foreign');
            $table->dropColumn('user_id');
            

            //Referencia lab users
            $table->bigInteger('laboratorios_user_id')->unsigned()->nullable();
            $table->foreign('laboratorios_user_id')->references('id')->on('laboratorios_user')->onDelete('cascade');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
