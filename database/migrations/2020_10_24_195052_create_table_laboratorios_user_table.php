<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLaboratoriosUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratorios_user', function (Blueprint $table) {
            $table->id();

            //Referencia users
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Referencia laboratorio
            $table->bigInteger('laboratorio_id')->unsigned()->nullable();
            $table->foreign('laboratorio_id')->references('id')->on('laboratorios')->onDelete('cascade');

            $table->string('proyect_id')->nullable();
            $table->string('proyect_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratorios_user');
    }
}
