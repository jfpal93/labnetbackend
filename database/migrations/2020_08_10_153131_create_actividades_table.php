<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->id();
            $table->string('estado')->nullable();
            $table->string('descripcion')->nullable();
            $table->decimal('puntaje',9,2);

            $table->timestamp("fecha_vencimiento")->nullable();
            $table->timestamp("fecha_completo")->nullable();

            //Referencia laboratorio
            $table->bigInteger('lab_id')->unsigned()->nullable();
            $table->foreign('lab_id')->references('id')->on('laboratorios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
