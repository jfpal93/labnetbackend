<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCalificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('calificaciones', function(Blueprint $table) {
            $table->dropForeign('calificaciones_user_id_foreign');
            $table->dropForeign('calificaciones_actividad_id_foreign');
            
            $table->dropColumn('user_id');
            $table->dropColumn('actividad_id');

            //Referencia actividad progreso
            $table->bigInteger('actividad_progreso_id')->unsigned()->nullable();
            $table->foreign('actividad_progreso_id')->references('id')->on('actividades_progreso')->onDelete('cascade');

 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
