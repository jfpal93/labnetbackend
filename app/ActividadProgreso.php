<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadProgreso extends Model
{
    //
    protected $table = 'actividades_progreso';

    protected $fillable = [
        'fecha_progreso',
        'resultado',
        'actividad_id',
        'laboratorios_user',
        'estado_progreso_id'];

    public function lab_user()
	{
		return $this->belongsTo('App\LaboratorioUser');
    }

    public function actividad()
	{
		return $this->belongsTo('App\Actividad');
    }

    public function estado_progreso()
	{
		return $this->belongsTo('App\EstadoProgreso');
    }
}
