<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaboratorioUser extends Model
{
    //
    protected $table = 'laboratorios_user';

    protected $fillable = [
        'user_id',
        'laboratorio_id',
        'proyect_id',
        'proyect_name'];

    public function laboratorio()
	{
		return $this->belongsTo('App\Laboratorio');
    }

    public function user()
	{
		return $this->belongsTo('App\User');
    }
}
