<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    //
    protected $table = 'laboratorios';

    protected $fillable = [
        'nombre',
        'descripcion',
        'activo',
        'calificacion',
        'curso_id'];

    public function curso()
	{
		return $this->belongsTo('App\Curso');
    }
    
    public function actividades(){
        return $this->hasMany('App\Actividad');
    }

    public function lab_users(){
        return $this->hasMany('App\LaboratorioUser');
    }
}