<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $table = 'cursos';

    protected $fillable = [
        'codigo',
        'descripcion',
        'ciclo_id'];


    public function ciclo()
	{
		return $this->belongsTo('App\Ciclo');
    }

    public function profesor(){
        return $this->belongsToMany('App\User', 'App\CursoUser','curso_id','user_id')->whereHas(
            'roles', function($q){
                $q->where('name', 'teachers');
            }
        );
    }

    public function estudiantes(){
        return $this->belongsToMany('App\User', 'App\CursoUser','curso_id','user_id')->whereHas(
            'roles', function($q){
                $q->where('name', 'student');
            }
        );
    }

    public function laboratorios(){
        return $this->hasMany('App\Laboratorio');
    }
}
