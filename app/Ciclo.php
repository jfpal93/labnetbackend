<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciclo extends Model
{
    //
    protected $table = 'ciclos';

    protected $fillable = [
        'termino',
        'ano_lectivo',
        'descripcion',
        'fechaIncio',
        'fechaFin'];

    public function cursos(){
        return $this->hasMany('App\Curso');
    }
}
