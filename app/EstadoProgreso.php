<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoProgreso extends Model
{
    //
    protected $table = 'estados_progreso';

    protected $fillable = [
        'estado',
        'descripcion'];
}
