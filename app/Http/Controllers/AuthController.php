<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Validator;
use App\User;
use App\Curso;
use App\Ciclo;
use Illuminate\Support\Facades\Crypt;
use App\Mail\AuthMail;
use Illuminate\Support\Facades\Mail;
use App\CursoUser;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AuthController extends Controller
{
    //

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        try {
            $credentials = request(['email', 'password']);
            $token = auth()->attempt($credentials);

            if ($token) {
                return $this->respondWithToken($token);
            }
            return response()->json(['error' => 401]);
        }
        catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

        
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            // 'lastname' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        $credentials = $request->only('email', 'password');

        if ($token = auth()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user=User::with('roles')->find(Auth::id());

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user
        ]);
    }

    public function getProfesores(){
        try {
            $ciclos = Ciclo::with('cursos','cursos.profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();

            return response()->json([
                'ciclos' => $ciclos,
                'profesores' => $profesores
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function attachProfToCourse(Request $request, $id){
        $curso = Curso::findOrFail($id);
        $prof = User::findOrFail($request->profId);
        $curso->profesor()->attach($prof);
        $curso->save();

        return $this->getProfesores();
    }

    public function createNbind(Request $request,$id){
        try {
            $pass=substr($request->name, 3)."".substr($request->lastname, 3)."".substr($request->telefono, -3)."";

            $user = new User();
            $user->name=$request->name;
            $user->lastname=$request->lastname;
            $user->email=$request->email;
            $user->password=bcrypt($pass);
            $user->telefono=$request->telefono;
            $user->save();

            $prof = Role::findByName("teachers");
            $user->assignRole($prof);
            $user->save();
            
            $curso=Curso::with('ciclo','profesor')->findOrFail($id);
            $curso->profesor()->attach($user);
            $curso->save();

            $ciclos = Ciclo::with('cursos','cursos.profesor')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            Mail::to($user->email)->send(new AuthMail($user,$pass));

            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function createProf(Request $request){
        try {
            $pass=substr($request->name, 3)."".substr($request->lastname, 3)."".substr($request->telefono, -3)."";

            $user = new User();
            $user->name=$request->name;
            $user->lastname=$request->lastname;
            $user->email=$request->email;
            $user->password=bcrypt($pass);
            $user->telefono=$request->telefono;
            $user->save();

            $prof = Role::findByName("teachers");
            $user->assignRole($prof);
            $user->save();

            Mail::to($user->email)->send(new AuthMail($user,$pass));

            return $this->getProfesores();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }

    }

    public function updateProf(Request $request, $id){
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());

            return $this->getProfesores();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deleteProf($id){
        try {
            User::destroy($id);
            return $this->getProfesores();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function unbind($id){
        try {
            $curso = Curso::findOrFail($id);
            $curso->profesor()->detach();
            $curso->save();
            
            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getStudents(){
        try {
            $ciclos = Ciclo::with('cursos','cursos.estudiantes','cursos.profesor')->get();
            $students = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'student');
                }
            )->get();

            return response()->json([
                'ciclos' => $ciclos,
                'students' => $students
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function deleteStudent($id){
        try {
            User::destroy($id);
            return $this->getStudents();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function createStudent(Request $request){
        try {
            $pass=substr($request->name, 3)."".substr($request->lastname, 3)."".substr($request->telefono, -3)."";

            $user = new User();
            $user->name=$request->name;
            $user->lastname=$request->lastname;
            $user->email=$request->email;
            $user->password=bcrypt($pass);
            $user->telefono=$request->telefono;
            $user->save();

            $prof = Role::findByName("student");
            $user->assignRole($prof);
            $user->save();

            Mail::to($user->email)->send(new AuthMail($user,$pass));

            return $this->getStudents();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function updateStudent(Request $request, $id){
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());

            return $this->getStudents();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
    public function attachStudentToCourse(Request $request, $id){
        $curso = Curso::findOrFail($id);
        $student = User::findOrFail($request->studentId);
        $curso->profesor()->attach($student);
        $curso->save();

        return $this->getStudents();
    }

    public function unbindStudentToCourse(Request $request,$id){
        try {
            $cursoUser = CursoUser::where(['user_id','===',$request->id,'curso_id','===',$id]);
            $cursoUser->destroy();
            
            return $this->getStudents();
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
