<?php

namespace App\Http\Controllers;

use App\Actividad;
use App\Curso;
use App\Ciclo;
use App\User;
use App\CursoUser;
use Illuminate\Http\Request;
use Exception;
use App\Laboratorio;
use Illuminate\Support\Facades\Auth;

class ActividadController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $actividades = Actividad::all();
            return $actividades;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $actividad = Actividad::create($request->all());
            // $laboratorio = Laboratorio::find($request->laboratorio_id);
            // $actividad->laboratorio()->save($laboratorio);
            return $this->getCoursesByTeacher();
            // return $request;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $actividad = Actividad::findOrFail($id);
            return $actividad;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $actividad = Actividad::findOrFail($id);
            $actividad->update($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Actividad::destroy($id);
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
        
    }

    function getCoursesByTeacher(){
        try {
            
            $cursosUser=CursoUser::where('user_id',Auth::id())->pluck('curso_id');

            $ciclos = Ciclo::with([
            'cursos'=> function($q) use($cursosUser) {
                $q->where('id', '=', $cursosUser); // '=' is optional
            }
            ,'cursos.profesor'
            ,'cursos.estudiantes',
            'cursos.laboratorios',
            'cursos.laboratorios.actividades'])
            ->get();
            return $ciclos;
        
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
