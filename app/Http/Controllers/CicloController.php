<?php

namespace App\Http\Controllers;

use App\Ciclo;
use App\Curso;
use Illuminate\Http\Request;

class CicloController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::all();
            return response()->json([
                'ciclos' => $ciclos,
                'cursos' => $cursos
            ]);

        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $ciclo = Ciclo::create($request->all());
            $ciclos = Ciclo::all();
            return $ciclos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ciclo  $ciclo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $ciclo = Ciclo::findOrFail($id);
            return $ciclo;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ciclo  $ciclo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $ciclo = Ciclo::findOrFail($id);
            $ciclo->update($request->all());
            $ciclos = Ciclo::all();
            return $ciclos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ciclo  $ciclo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Ciclo::destroy($id);
            $ciclos = Ciclo::all();
            return $ciclos;
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function attachCourseToCiclo(Request $request, $id){
        try {
            $ciclo = Ciclo::findOrFail($id);
            $curso = Curso::findOrFail($request->courseid);
            $curso->ciclo()->associate($ciclo);
            $curso->save();

            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::all();
            return response()->json([
                'ciclos' => $ciclos,
                'cursos' => $cursos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
