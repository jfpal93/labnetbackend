<?php

namespace App\Http\Controllers;

use App\Laboratorio;
use App\Curso;
use App\Ciclo;
use App\User;
use App\CursoUser;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Graze\TelnetClient\TelnetClient;
use App\LaboratorioUser;
use Illuminate\Support\Facades\Auth;
// use Borisuu\Telnet\TelnetClient;

class LaboratorioController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api',['except'=>['testGNS3','testGNS32']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $laboratorios = Laboratorio::all();
            return $laboratorios;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {

            // $laboratorio = Laboratorio::create($request->all());

            $lab = new Laboratorio();
            $lab->nombre=$request->nombre;
            $lab->descripcion=$request->descripcion;
            $lab->activo=$request->activo;
            $lab->calificacion=$request->calificacion;
            $lab->curso_id=$request->curso_id;
            $lab->save();

            $cursosUser=CursoUser::where('user_id',$request->user_id)->pluck('curso_id');

            $ciclos = Ciclo::with([
            'cursos'=> function($q) use($cursosUser) {
                $q->where('id', '=', $cursosUser); // '=' is optional
            }
            ,'cursos.profesor'
            ,'cursos.estudiantes',
            'cursos.laboratorios'])
            ->get();
            return $ciclos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laboratorio  $laboratorio
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $laboratorio = Laboratorio::findOrFail($id);
            return $laboratorio;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laboratorio  $laboratorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $laboratorio = Laboratorio::findOrFail($id);
            $laboratorio->update($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laboratorio  $laboratorio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Laboratorio::destroy($id);
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function testGNS3(){
        // $client = new GuzzleHttp\Client();
        // $res = $client->get('http://localhost:3080/v2/version');

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);
        // echo $res->getStatusCode(); // 200
        // echo $res->getBody();
        $response = $client->request('GET','projects');
        // $response = $client->request('POST','projects',
        // [
        //     'json' =>['name' => 'test']
        // ]);
        return $response->getBody();
    }

    public function testGNS32(){
        // $client = new GuzzleHttp\Client();
        // $res = $client->get('http://localhost:3080/v2/version');

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);
        // echo $res->getStatusCode(); // 200
        // echo $res->getBody();
        $response = $client->request('GET','appliances');
        // $response = $client->request('POST','projects',
        // [
        //     'json' =>['name' => 'test']
        // ]);
        return $response->getBody();
    }


    public function iniciarProyecto(Request $request){
        $curso=$request->curso_id;

        $user=User::find(Auth::id());
        $laboratorio = Laboratorio::findOrFail($request->lab_id);

        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('POST','projects',
        [
            'json' =>[
                'name' => $request->curso_id.'-'.$request->lab_id. '-'.$user->name.'-'.$user->lastname,
                // 'path' => '/home/gns3/GNS3/projects/lab'
                'auto_close'=>false
                ]
        ]);

        $contents=$response->getBody()->getContents(); 
        $resp =json_encode($contents);
        $arr = explode(',', $resp);
        $labuser=new LaboratorioUser();
        foreach($arr as $a){
            if(strpos($a, 'project_id')){
                $pjid = explode(':', $a);
                $encode=json_encode($pjid[1]);
                $labuser->proyect_id=str_replace('"', '',json_decode(stripslashes($encode)));
            }

            if(strpos($a, 'name')){
                $pjid = explode(':', $a);
                $encode=json_encode($pjid[1]);
                $labuser->proyect_name=str_replace('"', '',json_decode(stripslashes($encode)));
            }
        }
        
        $labuser->laboratorio()->associate($laboratorio);
        $labuser->user()->associate($user);
        $labuser->save();

        $laboratorio = Laboratorio::with(
            ['curso.laboratorios'=> function($q) use($curso) {
                $q->where('id', '=', $curso); // '=' is optional
            }
            ,'lab_users'])->find($request->lab_id);
        return $laboratorio;
    }

    function getCoursesByStudent($id){
        try {
            
            $cursosUser=CursoUser::where('user_id',$id)->pluck('curso_id');

            $ciclos = Ciclo::with([
            'cursos'=> function($q) use($cursosUser) {
                $q->where('id', '=', $cursosUser); // '=' is optional
            }
            ,'cursos.profesor',
            'cursos.laboratorios',
            'cursos.laboratorios.actividades',
            'cursos.laboratorios.lab_users'])
            ->get();
            return $ciclos;
        
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function continuarProyecto(Request $request){
        $user=User::find(Auth::id());
        $curso=$request->curso_id;

        $laboratorio = Laboratorio::with(
        ['curso.laboratorios'=> function($q) use($curso) {
            $q->where('id', '=', $curso); // '=' is optional
        }
        ,'lab_users'])->find($request->lab_id);

        $labUser=LaboratorioUser::where('user_id',Auth::id())
        ->where('laboratorio_id',$request->lab_id)->first();

        // $client = new Client([
        //     'base_uri' => 'http://www.simulabug.com:3080/v2/',
        // ]);
        // $response = $client->request('POST','projects/load',
        // [
        //     'json' =>['path' => '/'.$labUser->laboratorio_id.'/'.$request->curso_id.'-'.$request->lab_id.'-'.$user->name.'-'.$user->lastname.'.gns3']
        // ]);

        
        return $laboratorio;
    }


    public function nodoc7200(Request $request){

        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('POST','projects/'.$request->proyect_id.'/nodes',
        [
            'json' =>[
                
                    "symbol"=>":/symbols/router.svg",
                    "name"=>$request->name,
                    "properties"=>
                       [
                        "platform"=>"c7200",
                        "nvram"=>512,
                        "image"=>"c7200-advipservicesk9_li-mz.150-1.M.image",
                        "ram"=>512,
                        "slot3"=>"PA-GE",
                        "system_id"=>"FTX0945W0MY",
                        "slot0"=>"C7200-IO-FE",
                        "slot2"=>"PA-GE",
                        "slot4"=>"PA-GE",
                        "slot1"=>"PA-GE",
                        "idlepc"=>"0x606e0538"
                       ]
                    ,
                    "compute_id"=>"local",
                    "node_type"=>"dynamips"
                 
            ]
        ]);

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function getNodes($proyect_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('GET','projects/'.$proyect_id.'/nodes');

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function changeDevicePos($proyect_id,$node_id,Request $request){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('PUT','projects/'.$proyect_id.'/nodes'.'/'.$node_id,
        [
            'json' =>[
                
                "x"=>intval($request->x),
                "y"=>intval($request->y)
                
                 
            ]
        ]);

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function getLinks($proyect_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('GET','projects/'.$proyect_id.'/links');

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function getLabData($proyect_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('GET','projects/'.$proyect_id.'/links');

        $contents=$response->getBody()->getContents(); 
        

        $client2 = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response2 = $client2->request('GET','projects/'.$proyect_id.'/nodes');

        $contents2=$response2->getBody()->getContents(); 
        
        return response()->json([
            'nodes' => $contents2,
            'links' => $contents
        ]);
    }

    public function linkNodes($proyect_id,Request $request){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('POST','projects/'.$proyect_id.'/links',
        [
            'json' =>[
                'nodes' => $request->nodes
            ]
        ]);

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function deleteLink($proyect_id,$link_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('delete','projects/'.$proyect_id.'/links'.'/'.$link_id);

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function turnOn($proyect_id,$node_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('POST','projects/'.$proyect_id.'/nodes'.'/'.$node_id.'/start');

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function turnOff($proyect_id,$node_id){
        $client = new Client([
            'base_uri' => 'http://www.simulabug.com:3080/v2/',
        ]);
        $response = $client->request('POST','projects/'.$proyect_id.'/nodes'.'/'.$node_id.'/stop');

        $contents=$response->getBody()->getContents(); 
        
        return $contents;
    }

    public function OpenConsole(Request $request){
        $client = TelnetClient::factory();
        $dsn = '64.225.43.64:'.$request->port;
        $client->connect($dsn);

        // $command = 'enable';
        // $resp1 = $client->execute($command);

        // $command2 = 'configure terminal';
        // $resp2 = $client->execute($command2);

        $command3 = 'show interfaces';
        $resp3 = $client->execute($command3);

        return $resp3;

        // $telnet = new TelnetClient('64.225.43.64', $request->port);
        // $telnet->connect();
        // // $telnet->setPrompt('$');

        // $telnet->sendCommand('enable');
        // $cmdResult = $telnet->exec('show interfaces');
        // $telnet->disconnect();

        // return $cmdResult;
    }
}
