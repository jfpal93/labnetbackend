<?php

namespace App\Http\Controllers;

use App\EstadoActividad;
use Illuminate\Http\Request;

class EstadoActividadController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $estadoActividades = EstadoActividad::all();
            return $estadoActividades;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $estadoActividad = EstadoActividad::create($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoActividad  $estadoActividad
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $estadoActividad = EstadoActividad::findOrFail($id);
            return $estadoActividad;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoActividad  $estadoActividad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $estadoActividad = EstadoActividad::findOrFail($id);
            $estadoActividad->update($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoActividad  $estadoActividad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            EstadoActividad::destroy($id);
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
