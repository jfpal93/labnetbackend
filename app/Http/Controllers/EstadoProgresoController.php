<?php

namespace App\Http\Controllers;

use App\EstadoProgreso;
use Illuminate\Http\Request;

class EstadoProgresoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $estadoProgresos = EstadoProgreso::all();
            return $estadoProgresos;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $estadoProgreso = EstadoProgreso::create($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoProgreso  $estadoProgreso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $estadoProgreso = EstadoProgreso::findOrFail($id);
            return $estadoProgreso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoProgreso  $estadoProgreso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $estadoProgreso = EstadoProgreso::findOrFail($id);
            $estadoProgreso->update($request->all());
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoProgreso  $estadoProgreso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            EstadoProgreso::destroy($id);
            // return response()->json(['ok' => 'registro eliminado con exito'], 200);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
