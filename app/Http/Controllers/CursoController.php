<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Ciclo;
use App\User;
use App\CursoUser;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo obtener registros, problema: '.$e->getMessage()], 500);
        }
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $curso = Curso::create($request->all());

            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $curso = Curso::findOrFail($id);
            return $curso;
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $curso = Curso::findOrFail($id);
            $curso->update($request->all());

            $ciclos = Ciclo::with('cursos','profesor')->get();
            $cursos = Curso::with('ciclo')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            Curso::destroy($id);

            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function createNbind(Request $request,$id){
        try {
            $curso = Curso::create($request->all());
            $ciclo=Ciclo::findOrFail($id);
            $curso->ciclo()->associate($ciclo);
            $curso->save();

            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function unbind($id){
        try {
            $curso = Curso::findOrFail($id);
            $curso->ciclo()->dissociate();
            $curso->save();
            
            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::all();
            return response()->json([
                'ciclos' => $ciclos,
                'cursos' => $cursos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function attachProfToCourse(Request $request, $id){
        try {
            $curso = Curso::findOrFail($id);
            $prof = User::findOrFail($request->profId);
            $curso->profesor()->attach($prof);
            $curso->save();

            $ciclos = Ciclo::with('cursos')->get();
            $cursos = Curso::with('ciclo','profesor')->get();
            $profesores = User::with('curso')->whereHas(
                'roles', function($q){
                    $q->where('name', 'teachers');
                }
            )->get();
            return response()->json([
                'cursos' => $cursos,
                'profesores' => $profesores,
                'ciclos'=>$ciclos
            ]);
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getCoursesByTeacher($id){
        try {
            
            $cursosUser=CursoUser::where('user_id',$id)->pluck('curso_id');

            $ciclos = Ciclo::with([
            'cursos'=> function($q) use($cursosUser) {
                $q->where('id', '=', $cursosUser); // '=' is optional
            }
            ,'cursos.profesor'
            ,'cursos.estudiantes',
            'cursos.laboratorios',
            'cursos.laboratorios.actividades'])
            ->get();
            return $ciclos;
        
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }

    public function getCoursesByStudent($id){
        try {
            
            $cursosUser=CursoUser::where('user_id',$id)->pluck('curso_id');

            $ciclos = Ciclo::with([
            'cursos'=> function($q) use($cursosUser) {
                $q->where('id', '=', $cursosUser); // '=' is optional
            }
            ,'cursos.profesor',
            'cursos.laboratorios',
            'cursos.laboratorios.actividades',
            'cursos.laboratorios.lab_users'])
            ->get();
            return $ciclos;
        
        }catch(Exception $e) {
            return response()->json(['error' => 'no se pudo realizar la transaccion, problema: '.$e->getMessage()], 500);
        }
    }
}
