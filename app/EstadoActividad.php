<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoActividad extends Model
{
    //
    protected $table = 'estados_actividades';

    protected $fillable = [
        'actividad_id',
        'estados_id'];

    public function actividad()
    {
        return $this->belongsTo('App\Actividad');
    }

    public function estado()
	{
		return $this->belongsTo('App\Estado');
    }
}

