<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    //
    protected $table = 'calificaciones';

    protected $fillable = [
        'actividad_progreso_id',
        'calificacion'];

    public function actividad_progreso()
    {
        return $this->belongsTo('App\ActividadProgreso');
    }
}
