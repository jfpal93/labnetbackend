<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class AuthMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $pass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$pass)
    {
        //
        $this->user=$user;
        $this->pass=$pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'rockerlp23@gmail.com';
        $name = 'Coordinación UG';
 
      $subject = 'Terminar con el registro';
 
      return $this->view('mails.registro.registro')
 
      ->from($address, $name)
 
      ->subject($subject)
      ->with(['user' => $this->user,'pass' => $this->pass]);
    }
}
