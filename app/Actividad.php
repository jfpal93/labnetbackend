<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    //
    protected $table = 'actividades';

    protected $fillable = [
        'nombre',
        'descripcion',
        'puntaje',
        'fecha_vencimiento',
        'fecha_completo',
        'laboratorio_id'];

    public function laboratorio()
	{
		return $this->belongsTo('App\Laboratorio');
    }
}
