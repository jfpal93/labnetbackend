<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table = 'comentarios';

    protected $fillable = [
        'comentario',
        'actividad_progreso_id'];

    public function actividad_progreso()
	{
		return $this->belongsTo('App\ActividadProgreso');
    }
}
