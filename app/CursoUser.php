<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoUser extends Model
{
    //
    protected $table = 'cursos_users';

    protected $fillable = [
        'user_id',
        'curso_id'];

    public function user()
	{
		return $this->belongsTo('App\User');
    }

    public function curso()
	{
		return $this->belongsTo('App\Curso');
    }
}
